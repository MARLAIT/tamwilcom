package ma.kmprojets.tamwilcom.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.kmprojets.tamwilcom.dto.AnnexeDetailRequestDto;
import ma.kmprojets.tamwilcom.model.AnnexeDetailsModel;
import ma.kmprojets.tamwilcom.service.AnnexeDetailService;
import ma.kmprojets.tamwilcom.service.AnnexeService;
import ma.kmprojets.tamwilcom.service.ProductService;

@RestController
@RequestMapping("/annexeDetail")
public class AnnexedetailsController {
	@Autowired
	AnnexeService annexeService;
	@Autowired
	ProductService productService;
	@Autowired
	AnnexeDetailService annexeDetailService;
	
	@PostMapping
	public ResponseEntity<AnnexeDetailsModel> postAnnexeDetail(@RequestBody AnnexeDetailRequestDto annexeDetailRequestDto) {
		AnnexeDetailsModel annexeDetailsModel= annexeDetailService.addAnnexeDetailModel(annexeDetailRequestDto);
		return new ResponseEntity<>(annexeDetailsModel,HttpStatus.OK);
	}
	@GetMapping("/{productId}/{numero}")
	public ResponseEntity<List<AnnexeDetailsModel>>  getAnnexeDetail(@PathVariable Long productId,@PathVariable Long numero){
		
		List<AnnexeDetailsModel> annexeDetailsModel=annexeDetailService.getAnnexesDetailByNumeroAndId(numero,productId);

		return new ResponseEntity<>(annexeDetailsModel,HttpStatus.OK);
	}
	@GetMapping("/getAll")
    public ResponseEntity<List<AnnexeDetailsModel>> getAnnexesDetails() {
        List<AnnexeDetailsModel> annexesDetails = annexeDetailService.getAnnexesDetail();
        return new ResponseEntity<>(annexesDetails, HttpStatus.OK);
    }
	@DeleteMapping("/{id}")
	public void deleteAnnexe(@PathVariable Long id){
		annexeDetailService.deleteAnnexeDetail(id);
		
	}

}
