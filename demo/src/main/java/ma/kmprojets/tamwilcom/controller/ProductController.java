package ma.kmprojets.tamwilcom.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ma.kmprojets.tamwilcom.dto.ProductRequestDto;
import ma.kmprojets.tamwilcom.model.ProductModel;
import ma.kmprojets.tamwilcom.service.ProductService;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	ProductService productService;

	@PostMapping
	public ResponseEntity<ProductModel> postProduct(@RequestBody ProductRequestDto productRequestDto) {
		ProductModel productModel = productService.addProduct(productRequestDto);
		return new ResponseEntity<>(productModel, HttpStatus.OK);
	}

	@GetMapping("/{code}")
	public ResponseEntity<ProductModel> getProduct(@PathVariable String code) {
		ProductModel productModel = productService.getProductByCode(code);
		return new ResponseEntity<>(productModel, HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<List<ProductModel>> getProducts() {
		List<ProductModel> product = productService.getProducts();
		return new ResponseEntity<>(product, HttpStatus.OK);
	}

	@DeleteMapping("/{code}")
	public ResponseEntity<ProductModel> deleteProduct(@PathVariable String code) {
		ProductModel productModel = productService.deleteProduct(code);
		return new ResponseEntity<>(productModel, HttpStatus.OK);

	}
	// reste edit : les champs modifiable

}
