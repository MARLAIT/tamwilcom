package ma.kmprojets.tamwilcom.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ma.kmprojets.tamwilcom.dto.AnnexeRequestDto;
import ma.kmprojets.tamwilcom.model.AnnexeModel;
import ma.kmprojets.tamwilcom.service.AnnexeService;
import ma.kmprojets.tamwilcom.service.ProductService;
@RestController
@RequestMapping("/annexe")
public class AnnexeController {
	@Autowired
	AnnexeService annexeService;
	@Autowired
	ProductService productService;
	
	@PostMapping
	public ResponseEntity<AnnexeModel> postAnnexe(@RequestBody AnnexeRequestDto annexeRequestDto) {
		AnnexeModel annexeModel= annexeService.addAnnexe(annexeRequestDto);
		return new ResponseEntity<>(annexeModel,HttpStatus.OK);
	}
	@GetMapping("/get/{productId}")
	public ResponseEntity<List<AnnexeModel>> getAnnexe(@PathVariable Long productId){
		
		List<AnnexeModel> annexeModel=annexeService.getAnnexesByProductId(productId);
		return new ResponseEntity<>(annexeModel,HttpStatus.OK);
	}
	@GetMapping("/getAll")
    public ResponseEntity<List<AnnexeModel>> getAnnexes() {
        List<AnnexeModel> annexes = annexeService.getAnnexes();
        return new ResponseEntity<>(annexes, HttpStatus.OK);
    }
	@DeleteMapping("/delete/{numero}")
	public void deleteAnnexe(@PathVariable Long numero){
		annexeService.deleteAnnexe(numero);
	}
}
