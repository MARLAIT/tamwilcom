package ma.kmprojets.tamwilcom.controller;




import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ma.kmprojets.tamwilcom.dto.CreditResponseDto;
import ma.kmprojets.tamwilcom.dto.ObjectResponseDto;
import ma.kmprojets.tamwilcom.model.AnnexeDetailsModel;
import ma.kmprojets.tamwilcom.service.AnnexeDetailService;
import ma.kmprojets.tamwilcom.service.CreditDetailService;

@RestController
@RequestMapping("/credit")
public class CreditDetailController {
	
	@Autowired
	CreditDetailService creditDetailService;
	@Autowired
	AnnexeDetailService annexeDetailService;
	 
	
//	@GetMapping
//	public ObjectResponseDto getCredit() {
//		
//		return creditDetailService.getCredit();
//	
//	}
//	@GetMapping(path= "/{username}")
//	public ResponseEntity<ObjectResponseDto> gettest(@PathVariable String username) {
//		return new ResponseEntity<ObjectResponseDto>(creditDetailService.getCredit(username), HttpStatus.OK);
//		
////		return "test de param Url ->->->"+username;
//	}

	@PostMapping
	public ResponseEntity<ObjectResponseDto> postCredit(@RequestBody List<CreditResponseDto>  creditResponseDtoLst) {
		
		return new ResponseEntity<ObjectResponseDto>(creditDetailService.addCredit(creditResponseDtoLst), HttpStatus.OK);
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_ATOM_XML_VALUE,MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<ObjectResponseDto> getcredits(@RequestParam(value = "page") int page,@RequestParam(value = "limit") int limit) {
		return new ResponseEntity<ObjectResponseDto>(creditDetailService.getCredits(page,limit), HttpStatus.OK);
	}
	@GetMapping(path= "/{code}")
	public ResponseEntity<ObjectResponseDto> getProduct(@PathVariable String code) {
		return new ResponseEntity<ObjectResponseDto>(creditDetailService.getCreditByCode(code), HttpStatus.OK);
	}

	
//	@GetMapping(path= "/{code}")
//	public ResponseEntity<ObjectResponseDto> getAnnexesDetailByNumero(@PathVariable Long code) {
//		return new ResponseEntity<ObjectResponseDto>(creditDetailService.getAnnexesDetailByNumero(code), HttpStatus.OK);
//	}
	
	@GetMapping("/{productId}/{numero}")
	public ResponseEntity<List<AnnexeDetailsModel>>  getAnnexeDetail(@PathVariable Long productId,@PathVariable Long numero){
		
		List<AnnexeDetailsModel> annexeDetailsModel=annexeDetailService.getAnnexesDetailByNumeroAndId(numero,productId);

		return new ResponseEntity<>(annexeDetailsModel,HttpStatus.OK);
	}
	
	@GetMapping("/getAll")
    public ResponseEntity<ObjectResponseDto> getAnnexes() {
        
        return new ResponseEntity<ObjectResponseDto>(creditDetailService.getCredits(), HttpStatus.OK);
    }
	
}
