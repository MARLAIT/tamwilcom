package ma.kmprojets.tamwilcom.service;

import java.util.List;
import ma.kmprojets.tamwilcom.dto.AnnexeDetailRequestDto;
import ma.kmprojets.tamwilcom.model.AnnexeDetailsModel;
public interface AnnexeDetailService {
	//AnnexeDetailsModel getAnnexeDetail();
	AnnexeDetailsModel addAnnexeDetailModel(AnnexeDetailRequestDto annexeDetailRequestDto);

	List<AnnexeDetailsModel> getAnnexesDetailById(Long id);
	AnnexeDetailsModel getAnnexeDetailByNumero(Long numero);
	AnnexeDetailsModel getAnnexeDetailById(Long id);
	void deleteAnnexeDetail(Long id);

	List<AnnexeDetailsModel> getAnnexesDetail();

	List<AnnexeDetailsModel> getAnnexesDetailByNumero(Long numero);

	List<AnnexeDetailsModel> getAnnexesDetailByNumeroAndId(Long numero, Long id);


	




}
