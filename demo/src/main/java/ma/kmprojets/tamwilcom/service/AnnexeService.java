package ma.kmprojets.tamwilcom.service;

import java.util.List;

import org.springframework.stereotype.Service;


import ma.kmprojets.tamwilcom.dto.AnnexeRequestDto;
import ma.kmprojets.tamwilcom.model.AnnexeModel;
@Service
public interface AnnexeService {
		AnnexeModel getAnnexeByNumero(Long numero);
		AnnexeModel getAnnexeByProductId(Long id);
		AnnexeModel addAnnexe(AnnexeRequestDto annexeRequestDto);
		List<AnnexeModel> getAnnexes();
		void deleteAnnexe(Long numero);
		List<AnnexeModel> getAnnexesByProductId(Long id);
		List<AnnexeModel> getAnnexesByAnnexeNumero(Long numero);
}
