package ma.kmprojets.tamwilcom.service;

import ma.kmprojets.tamwilcom.dto.ObjectResponseDto;

public interface CreditSummaryService {
	ObjectResponseDto getCreditSummary();

}
