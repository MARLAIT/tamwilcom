package ma.kmprojets.tamwilcom.service;

import java.util.List;
import org.springframework.stereotype.Service;
import ma.kmprojets.tamwilcom.dto.ProductRequestDto;
import ma.kmprojets.tamwilcom.model.ProductModel;

@Service
public interface ProductService {
	
	ProductModel addProduct(ProductRequestDto productRequestDto);
	
	ProductModel getProductById(Long productId);
	ProductModel getProductByCode(String productId);
	List<ProductModel> getProducts();
	ProductModel deleteProduct(String code);
	ProductModel updateProduct(Long productId,ProductRequestDto productRequestDto);


}
