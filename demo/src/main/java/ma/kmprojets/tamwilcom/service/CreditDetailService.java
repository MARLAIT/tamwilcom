package ma.kmprojets.tamwilcom.service;

import java.util.List;
import ma.kmprojets.tamwilcom.dto.CreditResponseDto;
import ma.kmprojets.tamwilcom.dto.ObjectResponseDto;
public interface CreditDetailService {
//	ObjectResponseDto getCredit(String username);
	ObjectResponseDto getCredits(int page,int size);
	ObjectResponseDto addCredit(List<CreditResponseDto> creditResponseDtoLst);
	ObjectResponseDto getCreditByCode(String code);
	ObjectResponseDto getCredits();
//	ObjectResponseDto getAnnexesDetailByNumero(Long numero);
	
	
}
