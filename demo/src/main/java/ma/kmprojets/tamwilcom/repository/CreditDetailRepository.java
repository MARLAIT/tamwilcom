package ma.kmprojets.tamwilcom.repository;

import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ma.kmprojets.tamwilcom.model.CreditDetail;
@Repository
public interface CreditDetailRepository extends PagingAndSortingRepository<CreditDetail, Long> {
    List<CreditDetail> findAll();
//    CreditResponseDto findByUsername(String username);
//    ObjectResponseDto getAnnexesDetailByNumero(Long numero);
}