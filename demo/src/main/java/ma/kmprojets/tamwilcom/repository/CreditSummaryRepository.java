package ma.kmprojets.tamwilcom.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ma.kmprojets.tamwilcom.model.CreditSummary;

@Repository
public interface CreditSummaryRepository extends CrudRepository<CreditSummary, Long>{

}
