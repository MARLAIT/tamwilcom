package ma.kmprojets.tamwilcom.repository;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import ma.kmprojets.tamwilcom.model.AnnexeModel;
public interface AnnexeRepository extends CrudRepository<AnnexeModel, Long> {
	AnnexeModel findByNumero(Long numero);
	List<AnnexeModel> findAll();
	List<AnnexeModel> findAllByProductId(Long productId);
	List<AnnexeModel> findAllByNumero(Long numero);

	AnnexeModel findByProductId(Long productId);
	AnnexeModel findByCode(Long Code);
	
	
}
