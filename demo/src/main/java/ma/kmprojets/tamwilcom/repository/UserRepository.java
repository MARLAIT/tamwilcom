//package ma.kmprojets.tamwilcom.repository;
//
//import java.util.List;
//
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//import ma.kmprojets.tamwilcom.model.UserModel;
//
//@Repository
//public interface UserRepository extends CrudRepository<UserModel, Long> {
//
//	UserModel findByUsername(String username);
//
//	List<UserModel> findAll();
//
//}
