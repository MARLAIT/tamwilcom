package ma.kmprojets.tamwilcom.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ma.kmprojets.tamwilcom.model.ProductModel;

@Repository
public interface ProductRepository extends CrudRepository<ProductModel, Long> {

	ProductModel findByName(String name);
	List<ProductModel> findAll() ;
	ProductModel findByCode(String code);
	
	
}
