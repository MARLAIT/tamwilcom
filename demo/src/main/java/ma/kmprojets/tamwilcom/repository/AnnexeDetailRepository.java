package ma.kmprojets.tamwilcom.repository;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import ma.kmprojets.tamwilcom.model.AnnexeDetailsModel;
public interface AnnexeDetailRepository extends CrudRepository<AnnexeDetailsModel, Long>  {
	AnnexeDetailsModel findAnnexeById(Long id);
	List<AnnexeDetailsModel> findAll();
	AnnexeDetailsModel findByAnnexeNumero(Long numero);
	AnnexeDetailsModel findByProductId(Long productId);
	List<AnnexeDetailsModel> findAllByAnnexeNumero(Long numero);
	List<AnnexeDetailsModel> findAllByAnnexeNumeroAndProductId(Long numero,Long productId);
	List<AnnexeDetailsModel> findAllByProductId(Long productId);
}
