package ma.kmprojets.tamwilcom.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
@Entity(name = "KMTAMWIL_CREDITDETAIL")
@IdClass(CreditDetailId.class)
public class CreditDetail implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private Long bankCode;
	@Id
	private Long productCode;
	@Id
	private Long creditCode;
	@Id
	private String annexeCode;
	@Id
	private String fieldPosition;
	private String fieldName;
	private String fieldType;
	private String fieldDefaultValue;
//	private String fieldValues;
	private String fieldValue;
	

}
