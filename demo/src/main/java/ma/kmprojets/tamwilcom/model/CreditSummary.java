package ma.kmprojets.tamwilcom.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
@Entity(name = "KMTAMWIL_CREDITSUMMARY")

@IdClass(CreditSummaryId.class)
public class CreditSummary implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	private Long bankCode;
	@Id
	private Long productCode;
	@Id
	private Long creditCode;
	@Id
	private String annexeCode;
	private Date date;
	private String status;

}
