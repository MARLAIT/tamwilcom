package ma.kmprojets.tamwilcom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name = "KMTAMWIL_ANNEXEDETAILS")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnnexeDetailsModel {
	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	private Long id;
	//@Id
	//@GeneratedValue (strategy=GenerationType.SEQUENCE,
	//	    generator="native")
	//@GenericGenerator(
	//	    		name = "native",
	//	    		strategy = "native"
	//	    		)
	private String champs;
	private Long position;
	private Long taille;
	private String valeurParDefaut;

	@Column(name = "annexe_numero")
	private Long annexeNumero;

	@Column(name = "product_id")
	private Long productId;
	
	@Column(name = "annexDt_value")
	private String annextDtValue;

}
