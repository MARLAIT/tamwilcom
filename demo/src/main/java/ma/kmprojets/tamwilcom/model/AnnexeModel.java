
package ma.kmprojets.tamwilcom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name = "KMTAMWIL_ANNEXE")

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnnexeModel {

	@Id
	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "numeroAnnexe")
	private Long numero;

	@Column(name = "codeAnnexe")
	private Long code;

	@Column(name = "nomAnnexe")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "product_id")
	private Long productId;
	
	@Column(name = "product_value")
	private String annextValue;

}
