package ma.kmprojets.tamwilcom.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
@Entity(name = "KMTAMWIL_PRODUIT")
public class ProductModel {
	@Id
	@GeneratedValue
	private Long id;
	@Column(unique = true)
	private String code;
	private String name;
	private String segment;
}
