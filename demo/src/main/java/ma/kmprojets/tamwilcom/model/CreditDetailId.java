package ma.kmprojets.tamwilcom.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data @NoArgsConstructor @AllArgsConstructor
public class CreditDetailId implements Serializable{
	 private static final long serialVersionUID = 1L;
	 private Long bankCode;
	 private Long productCode;
	 private Long creditCode;
	 private String annexeCode;
	 private String fieldPosition;

}
