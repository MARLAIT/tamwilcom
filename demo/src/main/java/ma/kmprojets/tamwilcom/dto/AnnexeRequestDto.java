package ma.kmprojets.tamwilcom.dto;

import lombok.Data;

@Data
public class AnnexeRequestDto {
	
	private String name;
	private String description;
	private String productCode;

}
