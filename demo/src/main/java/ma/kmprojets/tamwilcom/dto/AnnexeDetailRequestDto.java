package ma.kmprojets.tamwilcom.dto;

import lombok.Data;

@Data
public class AnnexeDetailRequestDto {
	private String champs;
	private Long position;
	private Long taille;
	private String valeurParDefaut;
	private Long annexeNumero;
	private String productCode;
}
