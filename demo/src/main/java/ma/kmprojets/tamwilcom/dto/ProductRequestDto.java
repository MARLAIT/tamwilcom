package ma.kmprojets.tamwilcom.dto;

import lombok.Data;

@Data
public class ProductRequestDto {
	private String code;
	private String segment;
	private String name;
	private String parentCode;
}
