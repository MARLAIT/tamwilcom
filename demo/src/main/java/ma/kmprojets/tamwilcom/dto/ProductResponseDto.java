package ma.kmprojets.tamwilcom.dto;
import lombok.Data;
@Data
public class ProductResponseDto {
	private Long id;
	private String code;
	private String segment;
	private String name;
}
