package ma.kmprojets.tamwilcom.dto;
import lombok.Data;
@Data
public class CreditSummaryResponseDto {
	private Long bankCode;
	private Long productCode;
	private Long creditCode;
	private String annexeCode;
	private String fieldPosition;
	private String fieldName;
	private String fieldType;
	private String fieldValues;
}
