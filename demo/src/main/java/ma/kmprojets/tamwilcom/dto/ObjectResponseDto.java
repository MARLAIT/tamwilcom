package ma.kmprojets.tamwilcom.dto;

import lombok.Data;

@Data
public class ObjectResponseDto {
	private String code;
	private String message;
	private Object body;
}
