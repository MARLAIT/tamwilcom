
package ma.kmprojets.tamwilcom.serviceImp;

import java.util.List;

import java.util.stream.Collectors; 
import java.util.stream.StreamSupport;

import org.springframework.beans.BeanUtils; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service; 
import org.springframework.transaction.annotation.Transactional;
import lombok.AllArgsConstructor;
import ma.kmprojets.tamwilcom.dto.AnnexeRequestDto;
import ma.kmprojets.tamwilcom.model.AnnexeDetailsModel;
import ma.kmprojets.tamwilcom.model.AnnexeModel;
import ma.kmprojets.tamwilcom.model.ProductModel;
import ma.kmprojets.tamwilcom.repository.AnnexeRepository;
import ma.kmprojets.tamwilcom.repository.ProductRepository; 
import ma.kmprojets.tamwilcom.service.AnnexeService;
import ma.kmprojets.tamwilcom.service.ProductService;

@Service
@Transactional
@AllArgsConstructor 
 public class AnnexeServiceImpl implements AnnexeService{
 
	 @Autowired 
	 private AnnexeRepository annexeRepository;
	 
	 @Autowired 
	 private ProductRepository productRepository;
	 @Autowired
	private ProductService productService;
	 
	@Override 
	public AnnexeModel addAnnexe(AnnexeRequestDto annexeRequestDto) {
		ProductModel productModel = productRepository.findByCode(annexeRequestDto.getProductCode());
		AnnexeModel annexeModel = new AnnexeModel();
				
		BeanUtils.copyProperties(annexeRequestDto, annexeModel );
		annexeModel.setCode(annexeModel.getNumero());
		AnnexeDetailsModel annexedetailModel=new AnnexeDetailsModel();
		annexedetailModel.setProductId(productModel.getId());
		annexeModel.setProductId(productModel.getId());
			return annexeRepository.save(annexeModel);
		 }
	
	  @Override 
	  public AnnexeModel getAnnexeByNumero(Long numero) { 
		  return annexeRepository.findByNumero(numero); }
	 
	  @Override 
	  public List<AnnexeModel> getAnnexes() { 
		  List<AnnexeModel> annexes =StreamSupport .stream(annexeRepository.findAll().spliterator(), false)
				  									.collect(Collectors.toList());
		  return annexes; 
		  }
 
	 @Override
	 public void deleteAnnexe(Long numero)
	 { 
		 AnnexeModel annexeModel=getAnnexeByNumero(numero);
		 annexeRepository.delete(annexeModel);
	 
	 }
	@Override
	public AnnexeModel getAnnexeByProductId(Long id) {
		ProductModel productModel=productService.getProductById(id);
		AnnexeModel annexeModel=annexeRepository.findByProductId(productModel.getId());
		return annexeModel;
		
	}
	 @Override 
	  public List<AnnexeModel> getAnnexesByProductId(Long id) { 
		  List<AnnexeModel> annexes =StreamSupport .stream(annexeRepository.findAllByProductId(id).spliterator(), false)
					.collect(Collectors.toList());;
		  return annexes; 
		  }
	 @Override 
	  public List<AnnexeModel> getAnnexesByAnnexeNumero(Long numero) { 
		  List<AnnexeModel> annexes =annexeRepository.findAllByNumero(numero);
		  return annexes; 
		  }
	
 

  }
