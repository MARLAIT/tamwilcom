package ma.kmprojets.tamwilcom.serviceImp;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.AllArgsConstructor;
import ma.kmprojets.tamwilcom.dto.ProductRequestDto;
import ma.kmprojets.tamwilcom.model.AnnexeDetailsModel;
import ma.kmprojets.tamwilcom.model.AnnexeModel;
import ma.kmprojets.tamwilcom.model.ProductModel;
import ma.kmprojets.tamwilcom.repository.AnnexeDetailRepository;
import ma.kmprojets.tamwilcom.repository.AnnexeRepository;
import ma.kmprojets.tamwilcom.repository.ProductRepository;
import ma.kmprojets.tamwilcom.service.ProductService;

@Service

@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private AnnexeRepository annexeRepository;

	@Autowired
	private AnnexeDetailRepository annexeDetailRepository;

	@Override
	public ProductModel getProductByCode(String code) {
		return productRepository.findByCode(code);
	}

	@Override
	public ProductModel getProductById(Long productId) {
		return productRepository.findById(productId)
				.orElseThrow(() -> new IllegalArgumentException("could not find product with id: " + productId));

	}

	@Override
	public ProductModel addProduct(ProductRequestDto productRequestDto) {

		ProductModel productModel = new ProductModel();
		BeanUtils.copyProperties(productRequestDto, productModel);
		
		productModel = productRepository.save(productModel);

		String parentCode = productRequestDto.getParentCode();
		ProductModel parentProduct = getProductByCode(parentCode);

		if(parentCode!=null) {
		List<AnnexeModel> annexeModels = annexeRepository.findAllByProductId(parentProduct.getId());
		for (AnnexeModel annexeModel : annexeModels) {

			AnnexeModel newAnnexeModel = new AnnexeModel();
			newAnnexeModel.setCode(annexeModel.getCode());
			newAnnexeModel.setDescription(annexeModel.getDescription());
			newAnnexeModel.setName(annexeModel.getName());
			newAnnexeModel.setProductId(productModel.getId());
			annexeRepository.save(newAnnexeModel);
			List<AnnexeDetailsModel> annexeDetailsModels = annexeDetailRepository.findAllByAnnexeNumero(annexeModel.getNumero());
			for (AnnexeDetailsModel annexeDetailsModel : annexeDetailsModels) {
				AnnexeDetailsModel newAnnexeDetailsModel = new AnnexeDetailsModel();
				
				newAnnexeDetailsModel.setChamps(annexeDetailsModel.getChamps());
				newAnnexeDetailsModel.setPosition(annexeDetailsModel.getPosition());
				newAnnexeDetailsModel.setTaille(annexeDetailsModel.getTaille());
				newAnnexeDetailsModel.setValeurParDefaut(annexeDetailsModel.getValeurParDefaut());
				
				
				newAnnexeDetailsModel.setProductId(newAnnexeModel.getProductId());
				newAnnexeDetailsModel.setAnnexeNumero(newAnnexeModel.getNumero());
				annexeDetailRepository.save(newAnnexeDetailsModel);
			}
		}
		}
		return productModel;

	}

	
	@Override
	public List<ProductModel> getProducts() {
		List<ProductModel> products = StreamSupport.stream(productRepository.findAll().spliterator(), false)
				.collect(Collectors.toList());
		return products;
	}

	@Override
	public ProductModel deleteProduct(String code) {
		ProductModel product = getProductByCode(code);
		productRepository.delete(product);
		return product;
	}

	@Transactional
	@Override
	public ProductModel updateProduct(Long productId, ProductRequestDto productRequestDto) {
		ProductModel productToEdit = getProductById(productId);
		// les champs  modifiables !!
		productToEdit.setName(productRequestDto.getName());
		return productToEdit;
	}

}
