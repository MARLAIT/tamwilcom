//package ma.kmprojets.tamwilcom.serviceImp;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.stereotype.Service;
//
//import ma.kmprojets.tamwilcom.dto.ObjectResponseDto;
//import ma.kmprojets.tamwilcom.dto.UserLoginResponseDto;
//import ma.kmprojets.tamwilcom.dto.UserRequestDto;
//import ma.kmprojets.tamwilcom.dto.UserResponseDto;
//import ma.kmprojets.tamwilcom.model.UserModel;
//import ma.kmprojets.tamwilcom.repository.UserRepository;
//import ma.kmprojets.tamwilcom.service.UserService;
//
//@Service
//public class UserServiceImp implements UserService {
//
//	@Autowired
//	private UserRepository userRepository;
//
//	@Autowired
//	private PasswordEncoder passwordEncoder;
//
//	@Override
//	public ObjectResponseDto createUser(UserRequestDto userRequestDto) {
//		ObjectResponseDto objectResponseDto = new ObjectResponseDto();
//		try {
//
//			UserModel userModel = new UserModel();
//			BeanUtils.copyProperties(userRequestDto, userModel);
//			userModel.setPassword(passwordEncoder.encode(userModel.getPassword()));
//			UserModel savedUserModel = userRepository.save(userModel);
//			UserResponseDto userResponseDto = new UserResponseDto();
//			BeanUtils.copyProperties(savedUserModel, userResponseDto);
//			objectResponseDto.setCode("0");
//			objectResponseDto.setBody(userResponseDto);
//		} catch (Exception e) {
//			objectResponseDto.setCode("-1");
//			objectResponseDto.setMessage(e.getMessage());
//		}
//		return objectResponseDto;
//	}
//
//	@Override
//	public ObjectResponseDto getUsers() {
//		ObjectResponseDto objectResponseDto = new ObjectResponseDto();
//		try {
//			List<UserModel> userModelList = userRepository.findAll();
//			List<UserResponseDto> userResponseDtoList = new ArrayList<UserResponseDto>();
//			for (UserModel userModel : userModelList) {
//				UserResponseDto userResponseDto = new UserResponseDto();
//				BeanUtils.copyProperties(userModel, userResponseDto);
//				userResponseDtoList.add(userResponseDto);
//			}
//			objectResponseDto.setCode("0");
//			objectResponseDto.setBody(userResponseDtoList);
//		} catch (Exception e) {
//			objectResponseDto.setCode("-1");
//			objectResponseDto.setMessage(e.getMessage());
//		}
//		return objectResponseDto;
//	}
//
//	@Override
//	public UserLoginResponseDto findUserByUsername(String username) {
//		UserModel searchedUserModel = userRepository.findByUsername(username);
//		if (searchedUserModel == null) {
//			return null;
//		}
//		UserLoginResponseDto userLoginResponseDto = new UserLoginResponseDto();
//		BeanUtils.copyProperties(searchedUserModel, userLoginResponseDto);
//		return userLoginResponseDto;
//	}
//
//}
