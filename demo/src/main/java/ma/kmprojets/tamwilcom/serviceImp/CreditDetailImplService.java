package ma.kmprojets.tamwilcom.serviceImp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ma.kmprojets.tamwilcom.dto.CreditResponseDto;
import ma.kmprojets.tamwilcom.dto.ObjectResponseDto;

import ma.kmprojets.tamwilcom.model.CreditDetail;


import ma.kmprojets.tamwilcom.repository.CreditDetailRepository;
import ma.kmprojets.tamwilcom.repository.ProductRepository;
import ma.kmprojets.tamwilcom.service.CreditDetailService;

@Service
public class CreditDetailImplService implements CreditDetailService {

	@Autowired
	private CreditDetailRepository creditDetailRepository;
	@Autowired
	private ProductRepository productRepository;

	
	@Override
	public ObjectResponseDto addCredit(List<CreditResponseDto> creditResponseDtoLst) {
		ObjectResponseDto objectResponseDto = new ObjectResponseDto();
		try {	
//			List<CreditResponseDto> creditDetailResponseDtList = new ArrayList<CreditResponseDto>();
			for (CreditResponseDto creditResponseDto : creditResponseDtoLst) {
				CreditDetail crd= new CreditDetail();
				BeanUtils.copyProperties(creditResponseDto, crd);
				creditDetailRepository.save(crd);
			}
			objectResponseDto.setCode("0");
//			objectResponseDto.setBody(creditDetailResponseDtList);
		} catch (Exception e) {
			objectResponseDto.setCode("-1");
			objectResponseDto.setMessage(e.getMessage());
		}
		return objectResponseDto;
	}
//	@Override
//	public ObjectResponseDto getCredit() {
//		ObjectResponseDto objectResponseDto = new ObjectResponseDto();
//		try {
//
//			UserModel userModel = new UserModel();
//		
//			objectResponseDto.setCode("0");
//			objectResponseDto.setBody(userResponseDto);
//		} catch (Exception e) {
//			objectResponseDto.setCode("-1");
//			objectResponseDto.setMessage(e.getMessage());
//		}
//		return objectResponseDto;
//		
//	}
//	@Override
//	public ObjectResponseDto getCredit(String username) {
//		CreditResponseDto searchedCreditModel = creditDetailRepository.findByUsername(username);
//		ObjectResponseDto objectResponseDto = new ObjectResponseDto();
//		BeanUtils.copyProperties(searchedCreditModel, objectResponseDto);
//		
//		return objectResponseDto;
//	}
	@Override
	public ObjectResponseDto getCredits(int page,int size) {
		ObjectResponseDto objectResponseDto = new ObjectResponseDto();
		try {
			Pageable pageableRequest=PageRequest.of(page, size);
			Page<CreditDetail> creditDetaillList = creditDetailRepository.findAll(pageableRequest);
			
			List<CreditResponseDto> CreditResponseDtoList = new ArrayList<CreditResponseDto>();
			
			List<CreditDetail> creditDetaillList2=creditDetaillList.getContent();
			for (CreditDetail creditDetail : creditDetaillList2) {
				CreditResponseDto creditResponseDto = new CreditResponseDto();
				BeanUtils.copyProperties(creditDetail, creditResponseDto);
				CreditResponseDtoList.add(creditResponseDto);
			}
			objectResponseDto.setCode("0");
			objectResponseDto.setBody(creditDetaillList2);
		} catch (Exception e) {
			objectResponseDto.setCode("-1");
			objectResponseDto.setMessage(e.getMessage());
		}
		return objectResponseDto;
	}

	@Override
	public ObjectResponseDto getCreditByCode(String code) {
		ObjectResponseDto objectResponseDto = new ObjectResponseDto();
		try {
			objectResponseDto.setCode("0");
			objectResponseDto.setBody(productRepository.findByCode(code));
		} catch (Exception e) {
			objectResponseDto.setCode("-1");
			objectResponseDto.setMessage(e.getMessage());
		}
		return objectResponseDto;
	}
//	 @Override 
//	  public ObjectResponseDto getAnnexesDetailByNumero(Long numero) { 
//		 ObjectResponseDto objectResponseDto = new ObjectResponseDto();
//			try {
//				List<AnnexeDetailsModel> annexesDetail =StreamSupport .stream(annexeDetailRepository.findAllByProductId(numero).spliterator(), false)
//						.collect(Collectors.toList());;
//				objectResponseDto.setCode("0");
//				objectResponseDto.setBody(annexesDetail);
//			} catch (Exception e) {
//				objectResponseDto.setCode("-1");
//				objectResponseDto.setMessage(e.getMessage());
//			}
//		  
//		  return objectResponseDto; 
//		  }

//	@Override
//	public ObjectResponseDto getCredit() {
//		return null;
//	}
//	
	@Override
	public ObjectResponseDto getCredits() {
			ObjectResponseDto objectResponseDto = new ObjectResponseDto();
		try {
			List<CreditDetail> creditDetails = StreamSupport.stream(creditDetailRepository.findAll().spliterator(), false)
					.collect(Collectors.toList());
			objectResponseDto.setCode("0");
			objectResponseDto.setBody(creditDetails);
		} catch (Exception e) {
			objectResponseDto.setCode("-1");
			objectResponseDto.setMessage(e.getMessage());
		}
		return objectResponseDto;
	}
}
