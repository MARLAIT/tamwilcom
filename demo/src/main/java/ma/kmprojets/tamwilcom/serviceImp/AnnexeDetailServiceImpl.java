package ma.kmprojets.tamwilcom.serviceImp;

import java.util.List;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.AllArgsConstructor;
import ma.kmprojets.tamwilcom.dto.AnnexeDetailRequestDto;
import ma.kmprojets.tamwilcom.model.AnnexeDetailsModel;
import ma.kmprojets.tamwilcom.model.AnnexeModel;
import ma.kmprojets.tamwilcom.model.ProductModel;
import ma.kmprojets.tamwilcom.repository.AnnexeDetailRepository;
import ma.kmprojets.tamwilcom.repository.AnnexeRepository;
import ma.kmprojets.tamwilcom.repository.ProductRepository;
import ma.kmprojets.tamwilcom.service.AnnexeDetailService;


@Service
@Transactional
@AllArgsConstructor 
public class AnnexeDetailServiceImpl  implements AnnexeDetailService{
	 @Autowired 
	 private AnnexeRepository annexeRepository;
	 
	 @Autowired 
	 private ProductRepository productRepository;
	 @Autowired 
	 private AnnexeDetailRepository annexeDetailRepository;
	
	@Override
	public AnnexeDetailsModel addAnnexeDetailModel(AnnexeDetailRequestDto annexeDetailRequestDto) {
		AnnexeModel annexeModel=annexeRepository.findByNumero(annexeDetailRequestDto.getAnnexeNumero());
		ProductModel productModel = productRepository.findByCode(annexeDetailRequestDto.getProductCode());

		AnnexeDetailsModel annexeDetailModel=new AnnexeDetailsModel();
		BeanUtils.copyProperties(annexeDetailRequestDto, annexeDetailModel);
		
		annexeDetailModel.setProductId(productModel.getId());
		annexeDetailModel.setAnnexeNumero(annexeModel.getNumero());
		return annexeDetailRepository.save(annexeDetailModel);
		
		
	}

	@Override
	public AnnexeDetailsModel getAnnexeDetailById(Long id) {
		return annexeDetailRepository.findById(id).orElseThrow(() ->
        new IllegalArgumentException("could not find annexe Detail with id: " + id));
	}

	@Override
	public AnnexeDetailsModel getAnnexeDetailByNumero(Long numero) {
		return annexeDetailRepository.findByAnnexeNumero(numero);
	}
	 @Override 
	  public List<AnnexeDetailsModel> getAnnexesDetailByNumero(Long numero) { 
		  List<AnnexeDetailsModel> annexesDetail =StreamSupport .stream(annexeDetailRepository.findAllByAnnexeNumero(numero).spliterator(), false)
					.collect(Collectors.toList());;
		  return annexesDetail; 
		  }
	 @Override
	 public void deleteAnnexeDetail(Long id)
	 { 
		 AnnexeDetailsModel annexedetailModel=getAnnexeDetailById(id);
		 annexeDetailRepository.delete(annexedetailModel);
	 
	 }

	@Override
	public List<AnnexeDetailsModel> getAnnexesDetail() {
		 List<AnnexeDetailsModel> annexeDetails =StreamSupport .stream(annexeDetailRepository.findAll().spliterator(), false)
					.collect(Collectors.toList());
		 	return annexeDetails; 
	}

	@Override
	public List<AnnexeDetailsModel> getAnnexesDetailById(Long id) {
		
		List<AnnexeDetailsModel> annexeDetails =StreamSupport .stream(annexeDetailRepository.findAllByProductId(id).spliterator(), false)
				.collect(Collectors.toList());
	 	return annexeDetails;
	}
	@Override
	public List<AnnexeDetailsModel> getAnnexesDetailByNumeroAndId(Long numero, Long id) {
		List<AnnexeDetailsModel> annexedetailModel=annexeDetailRepository.findAllByAnnexeNumeroAndProductId(numero, id);
		return annexedetailModel ;
	}

	

}
